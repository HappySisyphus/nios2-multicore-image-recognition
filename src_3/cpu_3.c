//#include "../src_0/ascii_gray.h"
#include "altera_avalon_mutex_regs.h"
#include "system.h"
#include <altera_avalon_mutex.h>
#include <stdio.h>
//#include "sys/alt_stdio.h"

// FIFO
#include "../src_0/memory_locations.h"
#include "altera_avalon_fifo.h"
#include "altera_avalon_fifo_regs.h"
#include "altera_avalon_fifo_util.h"

#define TRUE 1
#define XCORR_X XCORR_X3
#define XCORR_Y XCORR_Y3
#define XCORR_VALUE XCORR_VALUE3
#define CY 2 * CROP_SIZE / NUMBER_OF_SLAVES

extern void delay(int millisec);

alt_u8 *shift_ptr = (alt_u8 *)SHARED_ONCHIP_BASE;

void write_to_private_onchip(alt_u64 *base) {
  int i = 0;
  alt_u64 *temp = DATA_START;
  for (i = 0; i < 36 / 2; i++) {
    *temp++ = *base++;
    *temp++ = *base++;
    *temp++ = *base++;
    *temp++ = *base++;
    *temp++ = *base++;
    *temp++ = *base++;
    *temp++ = *base++;
    *temp++ = *base++;
  }
}
void grayscale_cpu3(alt_u8 *base, alt_u8 shift, alt_u8 cropSize,
                    alt_u8 imageWidth) {
  /*Neglecting the first byte and taking the shift
   into account*/
  base += shift;
  alt_u8 *temp = (DATA_START);
  alt_u8 red, green, blue;
  alt_u16 max_number_of_iterations =
      48*2; // Rows*coloums*3*(number of unrold loops)
  alt_u8 i = 0;
  alt_u32 *color_ptr = base;
  for (i = 0; i < max_number_of_iterations; i++) {
      if(shift == 0) {
          // PIXEL 0
          *temp++ = ((*color_ptr & 0xFF) >> 2) + ((*color_ptr & 0xFF) >> (1+8)) + ((*color_ptr & 0xFF) >> (2+16));
          // PIXEL 1
          *temp = ((*color_ptr & 0xFF) >> (24+2));
          color_ptr += 1;
          *temp += ((*color_ptr & 0xFF) >> 1) + ((*color_ptr & 0xFF) >> (2+8));
          *temp++;
          // PIXEL 2
          *temp = ((*color_ptr & 0xFF) >> (2+16)) + ((*color_ptr & 0xFF) >> (24+1));
          color_ptr += 1;
          *temp += ((*color_ptr & 0xFF) >> 2);
          *temp++;
          // PIXEL 3
          *temp++ = ((*color_ptr & 0xFF) >> (2+8)) + ((*color_ptr & 0xFF) >> (1+16)) + ((*color_ptr & 0xFF) >> (2+24));
          color_ptr += 1;
        }

      else if (shift == 1) {
          // PIXEL 0
          *temp++ = ((*color_ptr & 0xFF) >> (2+8)) + ((*color_ptr & 0xFF) >> (1+16)) + ((*color_ptr & 0xFF) >> (2+24));
          color_ptr += 1;
          // PIXEL 1
          *temp++ = ((*color_ptr & 0xFF) >> 2) + ((*color_ptr & 0xFF) >> (1+8)) + ((*color_ptr & 0xFF) >> (2+16));
          // PIXEL 2
          *temp = ((*color_ptr & 0xFF) >> (24+2));
          color_ptr += 1;
          *temp += ((*color_ptr & 0xFF) >> 1) + ((*color_ptr & 0xFF) >> (2+8));
          *temp++;
          // PIXEL 3
          *temp = ((*color_ptr & 0xFF) >> (2+16)) + ((*color_ptr & 0xFF) >> (24+1));
          color_ptr += 1;
          *temp += ((*color_ptr & 0xFF) >> 2);
          *temp++;
        }

      else if (shift == 2) {
          // PIXEL 0
          *temp = ((*color_ptr & 0xFF) >> (2+16)) + ((*color_ptr & 0xFF) >> (24+1));
          color_ptr += 1;
          *temp += ((*color_ptr & 0xFF) >> 2);
          *temp++;
          // PIXEL 1
          *temp++ = ((*color_ptr & 0xFF) >> (2+8)) + ((*color_ptr & 0xFF) >> (1+16)) + ((*color_ptr & 0xFF) >> (2+24));
          color_ptr += 1;
          // PIXEL 2
          *temp++ = ((*color_ptr & 0xFF) >> 2) + ((*color_ptr & 0xFF) >> (1+8)) + ((*color_ptr & 0xFF) >> (2+16));
          // PIXEL 3
          *temp = ((*color_ptr & 0xFF) >> (24+2));
          color_ptr += 1;
          *temp += ((*color_ptr & 0xFF) >> 1) + ((*color_ptr & 0xFF) >> (2+8));
          *temp++;
        }

      else {
          // PIXEL 0
          *temp = ((*color_ptr & 0xFF) >> (24+2));
          color_ptr += 1;
          *temp += ((*color_ptr & 0xFF) >> 1) + ((*color_ptr & 0xFF) >> (2+8));
          *temp++;
          // PIXEL 1
          *temp = ((*color_ptr & 0xFF) >> (2+16)) + ((*color_ptr & 0xFF) >> (24+1));
          color_ptr += 1;
          *temp += ((*color_ptr & 0xFF) >> 2);
          *temp++;
          // PIXEL 2
          *temp++ = ((*color_ptr & 0xFF) >> (2+8)) + ((*color_ptr & 0xFF) >> (1+16)) + ((*color_ptr & 0xFF) >> (2+24));
          color_ptr += 1;
          // PIXEL 3
          *temp++ = ((*color_ptr & 0xFF) >> 2) + ((*color_ptr & 0xFF) >> (1+8)) + ((*color_ptr & 0xFF) >> (2+16));
        }
    }
}

void xcorr2_with_offset(alt_u8 *image, alt_u8 x_size, alt_u8 y_size) {
  alt_u32 Im_y, Im_x;
  // This is not needed when it is fused with offset function
  //  alt_u32 *xcorr2_ptr = (alt_u32 *)XCORR_IM_MEM1; // xcorr2 is an array
  //  stored in

  alt_u32 max_temp = 0, offsetX = 0, offsetY = 0; // used for finding the offset
  for (Im_y = 0; Im_y < y_size - PAT_SIZE + 1; ++Im_y) {
    for (Im_x = 0; Im_x < x_size - PAT_SIZE + 1; ++Im_x) {
      // calculate xcorr
      alt_u32 sum = *(image + 2)      // 1st row
                    + *(image + 33)   // 2nd row
                    + *(image + 35)   // 2nd row
                    + *(image + 64)   // 3rd row
                    + *(image + 68)   // 3rd row
                    + *(image + 97)   // 4th row
                    + *(image + 99)   // 4th row
                    + *(image + 130); // 5th row
      if (sum > max_temp) {
        max_temp = sum;
        offsetX = Im_x;
        offsetY = Im_y;
      }
      image++;
      // This is not needed when it is fused with offset function
      //          *xcorr2_ptr++ = sum;
    }
    image += 4;
  }
  // Save the position with highest cross correlation
  alt_u32 *xcorr_res_ptr = XCORR_X;
  *xcorr_res_ptr++ = offsetX;
  *xcorr_res_ptr++ = offsetY + CY;
  *xcorr_res_ptr = max_temp;
}

int main(void) {
  altera_avalon_fifo_init(FIFO_0_IN_CSR_BASE, 0, 3, 10);
  altera_avalon_fifo_init(FIFO_0_OUT_CSR_BASE, 0, 3, 10);
  altera_avalon_fifo_init(FIFO_1_IN_CSR_BASE, 0, 3, 10);
  altera_avalon_fifo_init(FIFO_1_OUT_CSR_BASE, 0, 3, 10);
  alt_u8 iteration;
  // Initilize mutex
  alt_mutex_dev *mutex = altera_avalon_mutex_open(MUTEX_0_NAME);
  // CPU 3 READY
  altera_avalon_fifo_write_fifo(FIFO_1_IN_BASE, FIFO_1_IN_CSR_BASE, 1);
  while (1) {
    while (0 ==
           altera_avalon_fifo_read_fifo(FIFO_0_OUT_BASE, FIFO_0_OUT_CSR_BASE)) {
    }
    altera_avalon_mutex_lock(mutex, 1);
    write_to_private_onchip(CROP_IM_MEM3);
    altera_avalon_mutex_unlock(mutex);
    grayscale_cpu3(DATA_START, *(shift_ptr + 3), CROP_SIZE, CROP_SIZE);
    //     printAsciiChar(DATA_START, 32, 12);
    //     putchar('\n');

    xcorr2_with_offset(DATA_START, CROP_SIZE, ROWS_FOR_LOCAL);
    altera_avalon_fifo_write_fifo(FIFO_1_IN_BASE, FIFO_1_IN_CSR_BASE, 1);
  }
  while (1)
    ;
  return 0;
}
