#ifndef IL2212_MULTI_MEMORY_LOCATIONS_H
#define IL2212_MULTI_MEMORY_LOCATIONS_H

#include "system.h"

// USED FOR COMMUNICATION
#define IM_PART1 1
#define IM_PART2 2
#define IM_PART3 3
#define IM_PART4 4

// ########################################
// #          SHARED ONCHIP               #
// ########################################

#define ONCHIP_BASE 0x2000

// values correspond to a 32x32x3 image (i.e already croped)
// IMAGE
#define NUMBER_OF_SLAVES      4
#define BYTES_PER_INT         4
#define IM_INFO_SIZE          4
#define BYTES_PER_PIXEL       3
#define CROP_SIZE             32
#define ONCHIP_SIZE_VALUE     8192
#define CROP_IM_SIZE          (CROP_SIZE*CROP_SIZE*BYTES_PER_PIXEL)
#define SPLIT_CROP_IM_SIZE    (CROP_IM_SIZE/NUMBER_OF_SLAVES)

// GRAYSCALE IMAGE
#define CROP_GREY_SIZE        (CROP_SIZE*CROP_SIZE)
#define SPLIT_CROP_GREY_SIZE  (CROP_GREY_SIZE/NUMBER_OF_SLAVES)

// XCORR2
#define PAT_SIZE 5
#define XCORR_ROW_SIZE        (CROP_SIZE - PAT_SIZE + 1)
#define XCORR_SIZE            (XCORR_ROW_SIZE*XCORR_ROW_SIZE)
#define XCORR_IM_SIZE         (XCORR_ROW_SIZE*XCORR_ROW_SIZE*BYTES_PER_INT)
#define SPLIT_XCORR_IM_SIZE   (XCORR_IM_SIZE/NUMBER_OF_SLAVES)


// IMAGE DATA (x_size, y_size, max_value, shift)
#define IM_INFO               SHARED_ONCHIP_BASE
// IMAGE STARTING POSITION IN MEMORY
#define IM_START_MEM          (IM_INFO+IM_INFO_SIZE)
// IMAGE STARTING POSITION IN MEMORY FOR EACH SEGMENT
#define CROP_IM_MEM1          (IM_START_MEM)
#define CROP_IM_MEM2          (CROP_IM_MEM1 + SPLIT_CROP_IM_SIZE)
#define CROP_IM_MEM3          (CROP_IM_MEM2 + SPLIT_CROP_IM_SIZE)
#define CROP_IM_MEM4          (CROP_IM_MEM3 + SPLIT_CROP_IM_SIZE)

// GREYSCALE IMAGE STARTING POSITION IN MEMORY FOR EACH SEGMENT
#define GREY_IM_MEM1          (CROP_IM_MEM4 + SPLIT_CROP_IM_SIZE)
#define GREY_IM_MEM2          (GREY_IM_MEM1 + SPLIT_CROP_GREY_SIZE)
#define GREY_IM_MEM3          (GREY_IM_MEM2 + SPLIT_CROP_GREY_SIZE)
#define GREY_IM_MEM4          (GREY_IM_MEM3 + SPLIT_CROP_GREY_SIZE)

// XCORR IMAGE STARTING POSITION IN MEMORY FOR EACH SEGMENT
#define XCORR_IM_MEM1         (GREY_IM_MEM4 + SPLIT_CROP_GREY_SIZE)
#define XCORR_IM_MEM2         (XCORR_IM_MEM1 + SPLIT_XCORR_IM_SIZE)
#define XCORR_IM_MEM3         (XCORR_IM_MEM2 + SPLIT_XCORR_IM_SIZE)
#define XCORR_IM_MEM4         (XCORR_IM_MEM3 + SPLIT_XCORR_IM_SIZE)

// RESULT OF XCORR
#define XCORR_X1              (XCORR_IM_MEM4 + SPLIT_XCORR_IM_SIZE)
#define XCORR_Y1              (XCORR_X1 + BYTES_PER_INT)
#define XCORR_VALUE1          (XCORR_Y1 + BYTES_PER_INT)

#define XCORR_X2              (XCORR_VALUE1 + BYTES_PER_INT)
#define XCORR_Y2              (XCORR_X2 + BYTES_PER_INT)
#define XCORR_VALUE2          (XCORR_Y2 + BYTES_PER_INT)

#define XCORR_X3              (XCORR_VALUE2 + BYTES_PER_INT)
#define XCORR_Y3              (XCORR_X3 + BYTES_PER_INT)
#define XCORR_VALUE3          (XCORR_Y3 + BYTES_PER_INT)

#define XCORR_X4              (XCORR_VALUE3 + BYTES_PER_INT)
#define XCORR_Y4              (XCORR_X4 + BYTES_PER_INT)
#define XCORR_VALUE4          (XCORR_Y4 + BYTES_PER_INT)


// ########################################
// #             LOCAL ONCHIP             #
// ########################################

#define ROWS_FOR_LOCAL        (CROP_SIZE / NUMBER_OF_SLAVES + PAT_SIZE - 1)
#define LOCAL_CROP_IM_SIZE    (CROP_SIZE * ROWS_FOR_LOCAL * BYTES_PER_PIXEL)
#define LOCAL_GREY_SIZE       (CROP_SIZE * ROWS_FOR_LOCAL)

#define DATA_START            (ONCHIP_BASE + ONCHIP_SIZE_VALUE - LOCAL_CROP_IM_SIZE)

// LOCAL IMAGE DATA (x_size, y_size, max_value, shift)
#define LOCAL_IM_INFO         (DATA_START)
#define LOCAL_IM_START_MEM    (LOCAL_IM_INFO + IM_INFO_SIZE)
#define LOCAL_CROP_IM_MEM     (LOCAL_IM_START_MEM)
#define LOCAL_GREY_IM_MEM     (LOCAL_CROP_IM_MEM + LOCAL_CROP_IM_SIZE)

#endif //IL2212_MULTI_MEMORY_LOCATIONS_H
