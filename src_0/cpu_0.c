/*CPU0 converts the set of RGB images stored on SRAM to grayscale and stores
the grayed image on the shared onchip-memory */
#include "altera_avalon_fifo.h"
#include "altera_avalon_fifo_regs.h"
#include "altera_avalon_fifo_util.h"
#include "altera_avalon_mutex.h"
#include "altera_avalon_mutex_regs.h"
#include "altera_avalon_performance_counter.h"
#include "altera_avalon_pio_regs.h"
#include "ascii_gray.h"
#include "images_aligned.h"
#include "memory_locations.h"
#include "io.h"
#include "system.h"
#include <stdio.h>
#define SECTION_1 1
#define TRUE 1
#define PERFORMANCE_MODE 1
#define DEBUG_MODE 0
#define OBJECT_POSITION_ADDRESS_START 0x103730

alt_u8 *imagePTR = SHARED_ONCHIP_BASE;
alt_u8 *objectCoordinatesPtr = (alt_u8 *)OBJECT_POSITION_ADDRESS_START;
alt_u8 objectPositionX;
alt_u8 objectPositionY;
alt_u8 object_positions[14];
alt_u8 *shift = (alt_u8 *)IM_INFO;
const alt_u8 shift_values[4] = {0, 3, 2, 1};

alt_u32 offset_x = 0;
alt_u32 offset_y = 0;
alt_u8 cX = 0;
alt_u8 cY = 0;
alt_u8 dSpan = 15;

/*FUNCTION DEFENITIONS*/
// 18 objX 	18 objY
// 23 objX 	18 objY
// 28 objX 	18 objY
// 33 objX 	18 objY
// 37 objX 	18 objY
// 41 objX 	18 objY
// 44 objX 	17 objY

//*Converts a RGB ppm3-image to grayscale and stores it on the shared onchip
// memory*//
// Formula for grayscale conversion is red*0.3125+green*0.5625+blue*0.125
// To avoid doing costly multiplication and instead use shifting
// operations one can write this in power of two as 1/4*red+1/16*red +
// 1/2*green+1/16*green+1/8*blue
void grayscale(alt_u8 *base) {
  alt_u8 imageWidth = *base++;
  alt_u8 imageHeight = *base++;
  alt_u32 i, j;
  alt_u8 *temp = SHARED_ONCHIP_BASE;
  base++;
  base++;
  alt_u8 red, green, blue;
  for (i = 0; i<(imageHeight * imageWidth)>> 3; i++) {
    red = *base++;
    green = *base++;
    blue = *base++;
    *temp++ =
        (red >> 2) + (red >> 4) + (green >> 1) + (green >> 4) + (blue >> 3);
    red = *base++;
    green = *base++;
    blue = *base++;
    *temp++ =
        (red >> 2) + (red >> 4) + (green >> 1) + (green >> 4) + (blue >> 3);
    red = *base++;
    green = *base++;
    blue = *base++;
    *temp++ =
        (red >> 2) + (red >> 4) + (green >> 1) + (green >> 4) + (blue >> 3);
    red = *base++;
    green = *base++;
    blue = *base++;
    *temp++ =
        (red >> 2) + (red >> 4) + (green >> 1) + (green >> 4) + (blue >> 3);
    red = *base++;
    green = *base++;
    blue = *base++;
    *temp++ =
        (red >> 2) + (red >> 4) + (green >> 1) + (green >> 4) + (blue >> 3);
    red = *base++;
    green = *base++;
    blue = *base++;
    *temp++ =
        (red >> 2) + (red >> 4) + (green >> 1) + (green >> 4) + (blue >> 3);
    red = *base++;
    green = *base++;
    blue = *base++;
    *temp++ =
        (red >> 2) + (red >> 4) + (green >> 1) + (green >> 4) + (blue >> 3);
    red = *base++;
    green = *base++;
    blue = *base++;
    *temp++ =
        (red >> 2) + (red >> 4) + (green >> 1) + (green >> 4) + (blue >> 3);

  }
}



void performance_test() {
  PERF_RESET(PERFORMANCE_COUNTER_0_BASE);
  /* Start Measuring */
  PERF_START_MEASURING(PERFORMANCE_COUNTER_0_BASE);
  PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, SECTION_1);
    // TEST HERE 
    burst_read_aligned(image_sequence[0], SHARED_ONCHIP_BASE, 48, 0, 0, 32,
    32);
    grayscale_shared(SHARED_ONCHIP_BASE, 0, 0,
    0,32,32); alt_u8 pattern[5 * 5] = {0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0,
                             0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0};
    xcorr2((alt_u8 *)SHARED_ONCHIP_BASE, pattern, 32, 32);
  
   PERF_END(PERFORMANCE_COUNTER_0_BASE, SECTION_1);
   perf_print_formatted_report(PERFORMANCE_COUNTER_0_BASE, alt_get_cpu_freq(), 1,
                              "SECTION 1");
   PERF_STOP_MEASURING(PERFORMANCE_COUNTER_0_BASE);
}

void burst_read_aligned(alt_u8 *im_ptr, alt_u8 *shared_image, alt_u8 im_x,
                        alt_u8 crop_start_x, alt_u8 crop_start_y, alt_u8 crop_x,
                        alt_u8 crop_y) {

  alt_u64 *shared_image64 = (alt_u64 *)shared_image;
  alt_u64 *image = (alt_u64 *)im_ptr;
  // store information int (first int, ppm format)
  *shared_image64 = *image;
 
 

  // Store the shift value to the shared onchip memory
  *(shift + 3) = shift_values[(0x3 & crop_start_x)];

 
 
  alt_u16 i, y_offset = 0;
  for (i = 0; i < crop_start_y; ++i) {
    y_offset += im_x + im_x + im_x;
  }
  image = (alt_u64 *)(im_ptr + y_offset + crop_start_x + crop_start_x +
                      crop_start_x);
 
  image++;
  shared_image64++;

  // Calculate how much you must add to go to next row
  alt_u8 temp = im_x - crop_x;
  alt_u8 add_for_next_row = (temp + temp + temp) >> 3;


  alt_u16 loopX = (crop_x + crop_x + crop_x) >> 3; // crop_x*3/4;
  alt_u8 x, y;
  for (y = 0; y < crop_y; ++y) {
    for (x = 0; x < loopX; ++x) {
      *shared_image64++ = *image++;
    }
    image = image + add_for_next_row;
  }
}

void calcPos(alt_u8 offset_x, alt_u8 offset_y, alt_u8 cropCoordX,
             alt_u8 cropCoordY) {
  objectPositionX = cropCoordX + offset_x + 2;
  objectPositionY = cropCoordY + offset_y + 2;
}


void calcCoord(alt_u8 prevX, alt_u8 prevY, alt_u8 imageSizeX,
               alt_u8 imageSizeY) {
  // Calculate next x-coordinate
  if (prevX <= dSpan) {
    cX = 0;
  } else if (prevX > imageSizeX - dSpan) {
    cX = imageSizeX - CROP_SIZE - 1;

  } else {
    cX = prevX - dSpan - 1;
  }

  if (prevY <= dSpan) {
    cY = 0;
  } else if (prevY > imageSizeY - dSpan) {
    cY = imageSizeY - CROP_SIZE - 1;
  } else {
    cY = prevY - dSpan - 1;
  }
}

int main() {

  /*INIT FIFOS*/
  altera_avalon_fifo_init(FIFO_0_IN_CSR_BASE, 0, 3, 10);
  altera_avalon_fifo_init(FIFO_0_OUT_CSR_BASE, 0, 3, 10);
  altera_avalon_fifo_init(FIFO_1_IN_CSR_BASE, 0, 3, 10);
  altera_avalon_fifo_init(FIFO_1_OUT_CSR_BASE, 0, 3, 10);
  alt_u8 i;
  alt_u8 firstTime = 0;
  alt_u32 numberOfIterations;
  alt_u8 imageSizeX = 48;
  alt_u8 imageSizeY = 36;
  alt_u8 accumulator = 0;

  while (accumulator != 4) {
    accumulator +=
        altera_avalon_fifo_read_fifo(FIFO_1_OUT_BASE, FIFO_1_OUT_CSR_BASE);
  }
  
#if PERFORMANCE_MODE
  PERF_RESET(PERFORMANCE_COUNTER_0_BASE);
  /* Start Measuring */
  PERF_START_MEASURING(PERFORMANCE_COUNTER_0_BASE);
  PERF_BEGIN(PERFORMANCE_COUNTER_0_BASE, SECTION_1);
  for (numberOfIterations = 0; numberOfIterations < 100; numberOfIterations++) {
#endif
    for (i = 1; i < sequence_length; i++) {
      /*READ THE OBJECT COORDINATES*/
      if (i == 1) {
        objectPositionX = 18;
        objectPositionY = 18;
      }
      calcCoord(objectPositionX, objectPositionY, imageSizeX, imageSizeY);
    

      burst_read_aligned(image_sequence[i], SHARED_ONCHIP_BASE, imageSizeX, cX,
                         cY, CROP_SIZE, CROP_SIZE);

      //*Writing to a fifo which can be read by all the processors.*//
      altera_avalon_fifo_write_fifo(FIFO_0_IN_BASE, FIFO_0_IN_CSR_BASE,
                                    IM_PART1);
      altera_avalon_fifo_write_fifo(FIFO_0_IN_BASE, FIFO_0_IN_CSR_BASE,
                                    IM_PART1);
      altera_avalon_fifo_write_fifo(FIFO_0_IN_BASE, FIFO_0_IN_CSR_BASE,
                                    IM_PART1);
      altera_avalon_fifo_write_fifo(FIFO_0_IN_BASE, FIFO_0_IN_CSR_BASE,
                                    IM_PART1);
      accumulator = 0;
      while (accumulator != 4) {
        accumulator +=
            altera_avalon_fifo_read_fifo(FIFO_1_OUT_BASE, FIFO_1_OUT_CSR_BASE);
      }

      alt_u32 *xcorr_ptr = (alt_u32 *)XCORR_VALUE1;
      alt_u32 *max_value_ptr = xcorr_ptr;

      xcorr_ptr = XCORR_VALUE2;
      if (*xcorr_ptr > *max_value_ptr) {
        max_value_ptr = xcorr_ptr;
      }
      xcorr_ptr = XCORR_VALUE3;
      if (*xcorr_ptr > *max_value_ptr) {
        max_value_ptr = xcorr_ptr;
      }

      xcorr_ptr = XCORR_VALUE4;
      if (*xcorr_ptr > *max_value_ptr) {
        max_value_ptr = xcorr_ptr;
      }
      offset_x = *(max_value_ptr - 2);
      offset_y = *(max_value_ptr - 1);
      calcPos(offset_x, offset_y, cX, cY);
      object_positions[i + i - 2] = objectPositionX;
      object_positions[i + i - 1] = objectPositionY;

    }
#if PERFORMANCE_MODE
  }
  PERF_STOP_MEASURING(PERFORMANCE_COUNTER_0_BASE);
  PERF_END(PERFORMANCE_COUNTER_0_BASE, SECTION_1);
  perf_print_formatted_report(PERFORMANCE_COUNTER_0_BASE, alt_get_cpu_freq(), 1,
                              "SECTION 1");
#endif

#if DEBUG_MODE
  for (i = 1; i < sequence_length; i++) {
    grayscale(image_sequence[i]);
    printAsciiHidden(SHARED_ONCHIP_BASE, 48, 36, object_positions[2 * i - 2],
                     object_positions[2 * i - 1], 4, 255);
  }
#endif
  while (1)
    ;

  while (TRUE) {
  }
}
