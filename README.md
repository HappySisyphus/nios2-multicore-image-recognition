# Introduction
Implementation of a streaming media application. The application loops over a sequence of PPM3 images and detects a given pattern in these.

# Hardware
 The hardware architecture is based around five [NIOS2](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/nios2/n2cpu-nii5v1gen2.pdf) CPU's. Each CPU have a individual 8kB on-chip memory and CPU0 is connected to a 8MB SDRAM. The architecture has two FIFOS and four hardware mutexes connected to a shared bus and can be used for synchronization. 

# Software
 The application is divided on the five CPU's. CPU_0 reads a cropped part of the image and transfers it to the shared onchip memory. CPU's 1-4 is then notidfied through a FIFO that data is avalible and then transfers thier part of the image to thier local onchip. The processors then perform image processing functions and saves the result to the shared onchip memory. 
![Schedule](./optimized_multicore_schedule.jpg)



